* Player Sprites by rvros: https://rvros.itch.io/animated-pixel-hero
* Slime Monster by rvros: https://rvros.itch.io/pixel-art-animated-slime
* Jungle Tileset by Jesse Munguia: https://jesse-m.itch.io/jungle-pack
* Arrow by kenam0: https://kenam0.itch.io/pixel-arrow