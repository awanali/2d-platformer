## Description
Prototype showcasing a 2D Platformer controller using a finite statemachine in Godot C#. 

## Preview
![preview](preview.gif)

## Features
* Idle
* Walk
* Run
* Jump
    * WallJump
    * AirJump
* Fall
* Slide
* WallSlide
* Crouch
* Hanging
* BowAttack
    * AirBowAttack
* SwordAttack with Combos
    * AirSwordAttack
* Hurt (Not implemented yet)
* Dash
* Dead (Not Implemented yet)
* Dodge (Animation only)
* Block (Animation only)
* Coyote Timer
* Jump Buffer
* Simple Enemy (cant damage player yet)

